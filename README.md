# docker

Repository of docker files used to automate various website related tasks (build, test, etc).

You can find the registry here: <https://gitlab.com/eyeo/websites/docker/container_registry>.
